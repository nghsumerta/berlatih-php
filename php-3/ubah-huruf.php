<?php
function ubah_huruf($string){
  $abjad = range('a', 'z');
  $simpan = "";

  for ($a=0; $a <strlen($string) ; $a++) {
    for ($b=0; $b <count($abjad) ; $b++) {
      if ($abjad[$b] == $string[$a]) {
        $simpan.= $abjad[$b+1];
      }
    }
  } echo  "$simpan <br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
